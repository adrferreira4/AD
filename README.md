**CDC - Confirmação de Dados Cadastrais**
======

Descrição: Retorna Nome da Pessoa

___
**Requisição SOAP/XML:**
>```
<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <PessoaFisicaSimplificada xmlns="SOAWebServices">
      <Credenciais>
        <Email>string</Email>
        <Senha>string</Senha>
      </Credenciais>
      <Documento>string</Documento>
      <DataNascimento>string</DataNascimento>
    </PessoaFisicaSimplificada>
  </soap:Body>
</soap:Envelope>
```

___
**Resposta SOAP/XML:**
>```
<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <PessoaFisicaSimplificadaResponse xmlns="SOAWebServices">
      <PessoaFisicaSimplificadaResult>
        <Documento>string</Documento>
        <Nome>string</Nome>
        <Mensagem>string</Mensagem>
        <Status>boolean</Status>
        <Transacao>
          <Status>boolean</Status>
          <CodigoStatus>string</CodigoStatus>
          <CodigoStatusDescricao>string</CodigoStatusDescricao>
        </Transacao>
      </PessoaFisicaSimplificadaResult>
    </PessoaFisicaSimplificadaResponse>
  </soap:Body>
</soap:Envelope>
```
___

**Modelo de Dados:**

|Tipo |Nome        |Descrição|
|:---:|:----------:|:---------------------------------------------------------------|
|1    |varchar(8)  |Documento	CPF da Pessoa                                       |
|2    |varchar(100)|Nome	Nome da Pessoa Física                                   |
|3    |varchar(2)  |Mensagem	Mensagem da transação(*)                            |
|4    |boolean     |Status	Status da transação(*)                                  |
|5    |**XML Node**  |Transacao	Node contendo mensagem padronizada sobre a transação.|
|5    |varchar(2)  |Status	Status da Transação                                     |
|5    |varchar(2)  |CodigoStatus	Código de Status                                |
|5    |varchar(2)  |CodigoStatusDescricao	Descrição do Codigo de Status           |